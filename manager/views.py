import psycopg2

from django.shortcuts import render
from django.views.generic import TemplateView

conn = psycopg2.connect(
    "dbname=linked_saas_new_server user=linkeduser password=88K2x5878a6d host=linkeddb.cnn70pzaec4g.us-east-1.rds.amazonaws.com")

cur = conn.cursor()
query = "select nspname from pg_catalog.pg_namespace;"
cur.execute(str(query))
all_schemas = []
for i in cur.fetchall():
    all_schemas.append(i[0])


class GeneralManager(TemplateView):
    template_name = 'index.html'

    def get_connection(self):
        conn = psycopg2.connect(
            "dbname=linked_saas_new_server user=linkeduser password=88K2x5878a6d host=linkeddb.cnn70pzaec4g.us-east-1.rds.amazonaws.com")

        cur = conn.cursor()
        return cur

    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        return render(request, self.template_name, {'all_queue': [],'all_schemas':all_schemas})

    def post(self, request, *args, **kwargs):
        queue = []
        tables = ['scraper_addaccountqueue','scraper_savedsearchqueue','scraper_salessendmessagequeue','scraper_salesnavigatorqueue','scraper_notificationqueue','scraper_linkedsendmessagequeue','scraper_linkedinqueue','scraper_connectionsendqueue']

        for table in tables:
            query = "select chrome_id,driver_id from linked_saas_new_server.%s.%s where status = '1'" % (request.POST.get('schema'),table)
            try:
                cur.execute(str(query))
                queue.append({'result':cur.fetchall(),'table':table})
            except:
                cur = self.get_connection()

        return render(request, self.template_name, {'all_queue': queue,'all_schemas':all_schemas})
